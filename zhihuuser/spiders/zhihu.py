# -*- coding: utf-8 -*-

import scrapy
from scrapy.utils.project import get_project_settings
from zhihuuser.items import UserItem
import json


class ZhihuSpider(scrapy.Spider):
    name = "zhihu"
    start_urls = ['http://www.zhihu.com/']

    #定义起始账号
    start_user = "laufei"
    settings = get_project_settings()

    #用户信息
    user_url = settings["USER_URL"]
    user_query = settings["USER_QUERY"]
    #关注列表
    follows_url = settings["FOLLOWS_URL"]
    follows_query = settings["FOLLOWS_QUERY"]
    #粉丝列表
    followers_url = settings["FOLLOWERS_URL"]
    followers_query = settings["FOLLOWERS_QUERY"]


    def start_requests(self):
        """
        用户Url与关注列表与粉丝列表
        
        """
        yield scrapy.Request(self.user_url.format(user=self.start_user, include=self.user_query),callback=self.parse_user)
        yield scrapy.Request(self.follows_url.format(user=self.start_user, include=self.followers_query, offset= 0, limit= 20), callback=self.parse_follows)
        yield scrapy.Request(self.followers_url.format(user=self.start_user, include=self.followers_query, offset= 0, limit= 20), callback=self.parse_followers)

    def parse_user(self, response):
        results = json.loads(response.text)

        item = UserItem()
        for field in item.fields:
            if field in results.keys():
                item[field] = results.get(field)
        yield item

        yield scrapy.Request(
            self.user_url.format(user=results.get("url_token"), include=self.follows_query),
            callback=self.parse_follows)

    def parse_follows(self, response):
        '''关注列表'''
        results = json.loads(response.text)

        if "data" in results.keys():
            for result in results.get("data"):
                yield scrapy.Request(self.user_url.format(user=result.get("url_token"), include=self.user_query),
                                     callback=self.parse_user)

        if "paging" in results.keys() and results.get("paging").get("is_end") == False:
            next_page = results.get("paging").get("next")
            yield scrapy.Request(next_page, callback=self.parse_follows)

    def parse_followers(self, response):
        '''粉丝列表'''
        results = json.loads(response.text)

        if "data" in results.keys():
            for result in results.get("data"):
                yield scrapy.Request(self.user_url.format(user=result.get("url_token"), include=self.user_query),
                                     callback=self.parse_user)

        if "paging" in results.keys() and results.get("paging").get("is_end") == False:
            next_page = results.get("paging").get("next")
            yield scrapy.Request(next_page, callback=self.parse_followers)